// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: [
    "@nuxtjs/tailwindcss",
    "@nuxtjs/strapi",
    "dayjs-nuxt",
    "nuxt-snackbar",
  ],
  strapi: {
    cookie: { maxAge: 60 * 60 * 24 * 7, path: "/" },
    // Need this `127.0.0.1` rather than `localhost`,
    // interesting bug: https://github.com/nuxt-modules/strapi/issues/229#issuecomment-1666854903
    url: "http://127.0.0.1:1337",
  },
  snackbar: {
    bottom: true,
    right: true,
    duration: 3000,
  },
});
